const gulp = require('gulp');
const rename = require('gulp-rename');

gulp.task('staging', function () {
    gulp.src('./src/environments/environment.staging.ts')
        .pipe(rename('environment.ts'))
        .pipe(gulp.dest('./src/environments/'));
});
gulp.task('minikube', function () {
    gulp.src('./src/environments/environment.minikube.ts')
        .pipe(rename('environment.ts'))
        .pipe(gulp.dest('./src/environments/'));
});
gulp.task('phoebe', function () {
    gulp.src('./src/environments/environment.phoebe.ts')
        .pipe(rename('environment.ts'))
        .pipe(gulp.dest('./src/environments/'));
});
gulp.task('pomona', function () {
    gulp.src('./src/environments/environment.pomona.ts')
        .pipe(rename('environment.ts'))
        .pipe(gulp.dest('./src/environments/'));
});
gulp.task('poseidon', function () {
    gulp.src('./src/environments/environment.poseidon.ts')
        .pipe(rename('environment.ts'))
        .pipe(gulp.dest('./src/environments/'));
});
