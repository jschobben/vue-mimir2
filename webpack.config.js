const path = require('path');
const nodeExternals = require('webpack-node-externals');
const HTMLWebpackPlugin = require('html-webpack-plugin');
let webpacK = require('webpack');


module.exports = env => {

    console.log('building with environment: ' + env.NODE_ENV);

    /**
     * the base settings of mimir in Vue.js with typescript
     */

    let config = {
        entry: './src/main.ts',
        output: {
            path: path.resolve(__dirname, 'dist'),
            filename: 'mimir.js'
        },
        module: {
            rules: [
                {
                    test: /\.vue$/,
                    loader: 'vue-loader',
                    options: {
                        'scss': 'vue-style-loader!css-loader!sass-loader',
                        'sass': 'vue-style-loader!css-loader!sass-loader?indentedSyntax'
                    }
                },
                {
                    test: /\.tsx?$/,
                    loader: 'ts-loader',
                    exclude: /node_modules/,
                    options: {
                        appendTsSuffixTo: [/\.vue$/]
                    }
                }
            ]
        },
        resolve: {
            extensions: ['.ts', '.js', '.vue', '.json'],
            alias: {
                'vue$': 'vue/dist/vue.esm.js',
                '@': path.resolve('src')
            }
        },
        devServer: {
            contentBase: './dist',
            port: 3000
        },
        performance: {
            hints: false
        }
    };


    // custom settings
    if (env.NODE_ENV === 'development' || env.NODE_ENV ==='staging') {
        config.plugins = [
            new HTMLWebpackPlugin({template: './src/index.html'})
        ]
    }
    return config;
};


