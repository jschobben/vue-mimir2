# Mimir in Vue
**This project is a temporary solution for organisation questions and 
person questions in "bewerken 2.0". It is written in VueJS with 
Typescript and WebPack**

The used tools are:

* VueJS <3
* Typescript for a "better" programming experience
* WebPack for bundeling
* Jest for unit testing (using jsdom as mock for the browser)
* Gulp for environment variables

### vue and typescript
Vue provides a Class-style component and "normal" single file components 
(SFC). Because of the lack of documentation of class style component, 
the good documentation of normal components and my proficiency
with Vue's normal templates I have decided to use them.

### Webpack
To keep the project organised the decision has been made to work with 
multiple small files. The files are bundled through webpack. Webpack
does the following things:
1. It translates vue SFC to Javascript (es5).
2. It translates Typescript to Javascript (es5).
3. bundles the files to one file (mimir.js).
4. Provides us with a development server.

#### webpack dev server
The webpack dev server is used to run the code in  a 
development environment. It is configured to open on port 3000 and 
reloads automatically when the code has been edited.

### environments
Environments in this component are almost the same as in angular. To use environment variables follow these steps:

1. place the variable as key in every `.ts` file in the `./environments` folder.
2. In your code use it by importing `./environments/environment.ts`

Under the hood gulp will overwrite this file based on your chosen env.

### Running VueJS
Some commands to start working with Vuejs

start a dev serrver with:
```bash
npm run serve
```
_It wil automatically open a browser with the site loaded (port 3000)_

To build an dev version of the application:
```bash
npm run build:dev
```
building the application:
```bash
npm run build
```

_You can also use `npm run build:staging` to build with staging variables._

_Both production and staging are 'production builds'._

testing the application:
```bash
npm test
```
_This will run jest_

### Implementing in webbased
3 steps are needed to load Vue into webbased.

1. Load the script
2. Define the scope for Vuejs.
3. Place the elements on the right position

##### load the scripts
Place the script from the ./dist folder in webbased en load it with a script tag (pretty obvious)

_Vue does not need to be loaded in._
##### define the scope
In order for vue to work it needs to have a 'scope' in which it is active. 
For this application it is an element (preferred a div) with the id: mimir-search-area

In order to set it surround the place in html where this code needs to be used in a div 
and give an ID of: mimir-search-area.

##### place the element on the right position

Place the `<vue-mimir-area></vue-mimir-area>` tag on the place where the code needs to live.

######configuring the element
Some information is required for the element to be used. 
(The element will not be rendered if some properties are not provided).
Properties that need te be provided are:
1. `type`. the type that needs to be searched for it must be a string that equals `'user'` or `'organisation'` .
2. `idFieldSelector` the selector of input where the result needs to be placed. **Note:** if using id it to start with an `#`. if using other selector name. the string must be: `[name="myInput"]`. The selector **must** return one element.






 
