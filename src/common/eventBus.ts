import Vue from 'vue';

/**
 *  this Vue instance is the way to communicate between sibling components. We are going data in it and listen to events.
 *  It's like using a Subject in Angular2+ in a service.
 *  eventBus is a common name for this object in the Vue world.
 */
export let eventBus = new Vue();