import {SearchRequest} from '../model/SearchRequest';
import {FilterAnswer} from '../model/FilterAnswer';
import {getCookie} from './cookie';
import {BaseResult} from '../model/BaseResult';


export let SEARCHREQUEST : SearchRequest = {
    from: 0,
    size: 100,
    query: undefined,
    filter: [],
    types: undefined,
    subtypes: undefined,
    onlyActive: true,
    suggestionMode: false
};