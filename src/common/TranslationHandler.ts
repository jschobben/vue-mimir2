import {getCookie} from './cookie';
import {eventBus} from './eventBus';
import {environment} from '../environments/environment';

export function makeTranslateAble(translations){
    translations.addresspost = translations["address"];
    translations.addressvisit = translations["siteaddress"];
    translations.phone = translations["phonenumber"];
    translations.fax = translations["faxnumber"];
    translations.url = translations["homepage"];
    translations.organisationcategory = translations["category"];
    translations.organisationcode = translations["organisation"];
    translations.sexe = translations["gender"];
    return translations;
}

export function getTranslations(){
    let xmlHttpRequest = new XMLHttpRequest();
    xmlHttpRequest.open("GET",environment.API_URL+"/search/i18n");
    xmlHttpRequest.setRequestHeader("Content-Type","application/json");
    xmlHttpRequest.setRequestHeader("Authorization",`Bearer ${getCookie(environment.ssoCookieName)}`);
    xmlHttpRequest.responseType = "json";
    xmlHttpRequest.onload = (): void => {
        eventBus.$data.translations = makeTranslateAble(xmlHttpRequest.response["translations"])
    };
    xmlHttpRequest.onerror = () => console.log(xmlHttpRequest.response);
    xmlHttpRequest.send();
}