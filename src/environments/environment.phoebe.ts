export const environment = {
    API_URL: 'https://api.dwmrs.rijksweb.nl',
    ssoCookieName: 'REGASToken'
};