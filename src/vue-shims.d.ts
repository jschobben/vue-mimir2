// in order to import .vue files in typescript this file is required

declare module '*.vue' {
    import Vue from 'vue';
    export default Vue;
}