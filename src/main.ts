import Vue from 'vue'
import SearchArea from '@/components/SearchArea.vue'

// the first property is the tag which is used in the HTML (in this case <counter></counter>)

Vue.component('vue-mimir-area', SearchArea);
new Vue({
    // the scope where the components lives in. needs te be checked with Code (now it lives inside the element with de id mimir-search-area)
    el: '#mimir-search-area'
});
