import {Registration} from './Registration';
import {Participant} from './Participant';

export interface Hit {
    /*
     common fields
     */
    type: string;
    id: number;
    active: boolean;
    /* common to person and items */
    registrations: Registration[];

    /* common to person and organisations */
    country: string;
    municipality: string;
    town: string;
    postal: string;
    email: string;
    phone: string;
    fax: string;
    organisationcode: string;

    /* common to organisations, usergroups and items */
    name: string;

    /* common to organisations and usergroups */
    members: string;
    systemroles: string;

    /*
     person specific fields
     */
    address: string;
    fullname: string;
    lastname: string;
    firstname: string;
    initials: string;
    sexe: string;
    dateofbirth: string;
    insertion: string;
    callname: string;
    title: string;
    mobile: string;
    persontype: string;
    bsn: string;
    birthcountry: string;
    birthplace: string;
    departement: string;

    /*
     organisation specific fields
     */
    url: string;
    description: string;
    organisationcategory: string;
    addresspost: string;
    addressvisit: string;

}