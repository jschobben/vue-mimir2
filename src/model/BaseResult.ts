export interface BaseResult {
    /**
     * Status of the result
     */
    statuscode: number;
    message: string;

    /**
     * Useful stuff for generating url's
     */
    registrationUrl: string;
    substitution: string;


}