export interface Participant {
    id: number;
    type: string;
    name: string;
    active: boolean;
    role: string;
}