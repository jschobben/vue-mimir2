import {FilterAnswer} from './FilterAnswer';

export interface SearchRequest {
    from: number;
    size: number;
    query: string;
    filter: FilterAnswer[];  //anders
    types: string[];
    subtypes: string[];
    onlyActive: boolean;
    suggestionMode: boolean;
}